Name:           pinephone-helpers
Version:        0.4.0
Release:        1%{?dist}
Summary:        Pinephone call audio routing for megis kernel

License:        GPLv3+
URL:            https://gitlab.com/fedora-mobile/%{name}

Source0:        pinephone-boot.service
Source1:        pp-start.sh
Source2:        pp-stop.sh
Source3:        20-pinephone.conf
Source4:        10-proximity.rules
Source5:        90-pinephone.conf
Source6:        pinephone.json

BuildRequires:  gcc
BuildRequires:  systemd-rpm-macros

Requires:       bash
Requires:       iio-sensor-proxy

%description
Turn on and off the modem automatically at boot, and listen for 
new calls to automatically configure audio routing when detected.

%global debug_package %{nil}

%install

# pinephone-boot.service
mkdir -p %{buildroot}/etc/systemd/system
cp %{SOURCE0} %{buildroot}/etc/systemd/system

# start/stop scripts
mkdir -p %{buildroot}/usr/bin
cp %{SOURCE1} %{buildroot}/usr/bin
cp %{SOURCE2} %{buildroot}/usr/bin

# pinephone conf
mkdir -p %{buildroot}/etc/NetworkManager/conf.d
cp %{SOURCE3} %{buildroot}/etc/NetworkManager/conf.d


# proximity rules
install -D -m 644 %{SOURCE4} %{buildroot}%{_udevrulesdir}/10-proximity.rules

# 90-pinephone.conf
mkdir -p %{buildroot}/etc/pulse/daemon.conf.d
cp %{SOURCE5} %{buildroot}/etc/pulse/daemon.conf.d/

# feedbackd theme
mkdir -p %{buildroot}/usr/share/feedbackd/themes
cp %{SOURCE6} -p %{buildroot}%{_datadir}/feedbackd/themes/


%post
%systemd_post pinephone-boot.service
systemctl enable pinephone-boot.service


%preun
%systemd_preun pinephone-boot.service

%postun
%systemd_postun_with_restart pinephone-boot.service

%files
%{_bindir}/pp-start.sh
%{_bindir}/pp-stop.sh
%{_sysconfdir}/systemd/system/pinephone-boot.service
%{_sysconfdir}/NetworkManager/conf.d/20-pinephone.conf
%dir %{_sysconfdir}/pulse/daemon.conf.d/
%{_sysconfdir}/pulse/daemon.conf.d/90-pinephone.conf

%dir %{_datadir}/feedbackd/themes/
%{_datadir}/feedbackd/themes/pinephone.json
%{_udevrulesdir}/10-proximity.rules

%changelog
* Tue Jan 31 2023 Tor - 0.4.0-1
- Remove alsa UCM. It has been upstreamed!

* Wed Sep 22 2021 Tor - 0.3.0-3
- Fix collision 

* Fri Sep 10 2021 Tor - 0.3.0-2
- Use link instead of copy file

* Fri Sep 10 2021 Tor - 0.3.0-1
- Update UCM Profiles

* Mon Aug 16 2021 Tor - 0.2.0-0
- Update UCM profiles

* Mon Feb 01 2021 Tor - 0.1.0-0
- Remove unused calls helpers

* Sat Jan 23 2021 Tor - 0.0.9-5
- Adding pinephone.json for feedbackd theme

* Thu Jan 14 2021 Tor - 0.0.9-4
- Update alsa ucm files

* Wed Jan 13 2021 Tor - 0.0.9-3
- Add missing updates.

* Tue Jan 12 2021 Yoda - 0.0.9-2
- Don't Require megi-kernel 

* Mon Jan 11 2021 Tor - 0.0.9-1
- Update pineaudio.c 

* Wed Dec 09 2020 Tor - 0.0.8-2
- Fix bad config files 

* Sat Dec 05 2020 Tor - 0.0.8-1
- Adding proximity rules file

* Sat Nov 21 2020 Tor - 0.0.7-1
- Adding Alsa configs for ucm2 

* Sat Oct 17 2020 Yoda - 0.0.6-1
- Pineaudio updated to match new kernel 5.9.0 . Minor addition to enable sound on boot.

* Fri Aug 21 2020 Tor - 0.0.5-1
- Update paths for new modem driver in megi kernel 5.8+

* Sat Feb 29 2020 Nikhil Jha <hi@nikhiljha.com> - 0.0.1-1
- Initial packaging
