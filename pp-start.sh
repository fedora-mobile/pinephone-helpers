#!/bin/sh

# Power up the modem
echo 1 > /sys/class/modem-power/modem-power/device/powered

echo 4350000 > /sys/class/power_supply/axp20x-battery/voltage_max_design

echo 1600000 > /sys/class/power_supply/axp20x-battery/constant_charge_current_max
echo 1400000 > /sys/class/power_supply/axp20x-battery/constant_charge_current

# Enable the modem
MODEM_ID=""

get_modem_id()
{
    MODEM_LIST="`mmcli -L | grep QUECTEL`"
    if [ "$MODEM_LIST" ]; then
        # mmcli output is "   /org/freedesktop/ModemManager1/Modem/MODEM_ID ..."
        # MODEM_PATH will store the D-Bus object path, from which we'll extract
        # MODEM_ID
        MODEM_PATH="`echo "$MODEM_LIST" | sed 's%[^/]*\(/[^ ]*\).*%\1%'`"
        MODEM_ID=`basename "$MODEM_PATH"`
    fi
}

# Wait for the modem to be available
while [ ! "$MODEM_ID" ]; do
    sleep 1
    get_modem_id
done

mmcli -em $MODEM_ID
